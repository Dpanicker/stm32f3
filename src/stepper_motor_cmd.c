#include <stdio.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h"
#include <stdlib.h>

extern int state;	
/* state is defined in main, so that even if we are using the 
command multiple times, the state will be remembered.*/
uint32_t step=0;

extern int delay();


int step_up(uint32_t steps,uint32_t clock,uint32_t direction)
{
	int step_value;
	step_value=abs(steps);
	for(int i=1;i<=step_value;i++){

		if(5==state) // resetting the state to initial state
			state=1;
		else if(0==state) // resetting the state to initial state
			state=4;

		/*
		   The below pattern used to change fields in coils.
		   We have basically four states.
		   Depending upon the previous state, the states will keep on changing.
		   Since we are using full step drive method, at a time two coils will be on.
		 */

		switch(state)
		{
			case 1 :
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_9,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_SET);
				delay(clock);
				break;
			case 2 :
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_9,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
				delay(clock);
				break;
			case 3:  
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_9,GPIO_PIN_SET);
				delay(clock);
				break;
			case 4:  
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_9,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_SET);
				delay(clock);
				break;
			default: 
				break;
		}
		if(direction==1)//motor moves in forward direction 
			state++;
		else if(direction == 0) //mo0tor moves in backward direction
			state--;
	}
	return 0;
}

void stepper_cmd(int mode)
{
	int rc=0;
	uint32_t steps=0;
	uint32_t clock=0,direction=0; 
	if (mode!=CMD_INTERACTIVE) return;

	rc = fetch_uint32_arg(&steps);
	if(rc) {
		printf("missing : no of steps \n");
		return;
	}
	rc = fetch_uint32_arg(&clock);
	if(rc) {
		printf("missing : delay between steps missing \n");
		return;
	}
	rc = fetch_uint32_arg(&direction);
	if(rc) {
		printf("missing : direction is missing \n");
		return;
	}

	//GPIO Initialization
	GPIO_InitTypeDef GPIO_InitStruct;
	/* Configure the GPIO pins for the UART */
	__GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 ;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	step_up(steps,clock,direction);

}
ADD_CMD("stepper",stepper_cmd,"          give 1. signed no for direction+no of steps 2. delay between clock steps  => direction 1 clockwise 2 anti-clockwise")

