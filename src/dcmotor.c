/* helped by: Manpreet Raju */



#include <stdio.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h"

#define PERIOD_VALUE 1000  		/* Period Value  */
#define PWM_ACCURACY 256		/* Pulse Width   */ 
#define DUTY_CYCLE_DEFAULT 128
#define SPEED_MIN 1
#define SPEED_MAX PWM_ACCURACY-1
#define DCM_START 0
#define DCM_ACTIVE 1


// Protytes for the functions used
void MotorStart(void);
void MotorStop(void);
void TimerPWM(void);
void Motor_Handler(void);
void MotorSpeedControl(void);
void ReadADC(void);
void TimerPWMPrint(void);


// variables used in functions
uint16_t ADCData;
uint16_t CurrentSpeedValue;
uint16_t speedDC;
uint32_t TIM15PrescalerValue = 0;
uint32_t TIM15ReloadRegValue = 0;
uint32_t ch1PulseValue = 0;


/* TIM15 used to generate PWM signals PB14 to Enable Motor on Pins PCO,PC1 and 1 ADC's for Tachometer: PB0 – ADC3_IN12 */

ADC_HandleTypeDef 	hadc;
ADC_ChannelConfTypeDef 	ADCchannel;
TIM_OC_InitTypeDef 	TimConfig;
TIM_HandleTypeDef 	TimHandle;

void CmdMTRInit(int mode)		/* command to initiate the timer and GPIO for Motor*/
{
  GPIO_InitTypeDef GPIO_InitStruct;

  if (mode != CMD_INTERACTIVE) 
      return;

  __HAL_RCC_ADC34_CONFIG(RCC_ADC34PLLCLK_DIV1);


/* Enable the clocks */
  __GPIOB_CLK_ENABLE();
  __GPIOC_CLK_ENABLE();
  __ADC34_CLK_ENABLE();
  __TIM15_CLK_ENABLE();



/* Config TIM15 CH1 on PB14  */
  
  GPIO_InitStruct.Pin =  GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = 1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); 

  /* Configure these Motor pins in digital mode using HAL_GPIO_Init() on PORT C */ 
  
  GPIO_InitStruct.Pin       = (GPIO_PIN_0 | GPIO_PIN_1 );
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;		/* Output with PushPull */
  GPIO_InitStruct.Pull      = GPIO_NOPULL;			/* No pull up mode activated */
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;		/* high speed configuration activated */
  GPIO_InitStruct.Alternate = 0;			/* All the alternate functions are off */
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct); 		

 /* Configure these ADC pins in analog mode using HAL_GPIO_Init() */
  GPIO_InitStruct.Pin       = GPIO_PIN_0;
  GPIO_InitStruct.Mode      = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = 0;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  

  /* Initialize ADC */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION12b;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc.Init.EOCSelection = EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.NbrOfConversion = 1;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.NbrOfDiscConversion = 0;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = 0;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = OVR_DATA_OVERWRITTEN;


 /*configure the selected channel*/

  ADCchannel.Channel 	   = ADC_CHANNEL_12;
  ADCchannel.Rank 	   = ADC_REGULAR_RANK_1;
  ADCchannel.SamplingTime  = ADC_SAMPLETIME_7CYCLES_5; // ADC_SAMPLETIME_19CYCLES_5 ADC_SAMPLETIME_7CYCLES_5
  ADCchannel.SingleDiff    = ADC_SINGLE_ENDED;
  ADCchannel.OffsetNumber  = ADC_OFFSET_NONE; // ADC_OFFSET_1
  ADCchannel.Offset 	   = 0;	



 /* Initialize Timer15 */
  TIM15PrescalerValue          = (uint32_t) (SystemCoreClock / (PERIOD_VALUE * PWM_ACCURACY)) - 1;
  TIM15ReloadRegValue          = PWM_ACCURACY;
  TimHandle.Instance           = TIM15;
  TimHandle.Init.Prescaler     = TIM15PrescalerValue;
  TimHandle.Init.Period        = TIM15ReloadRegValue;
  TimHandle.Init.ClockDivision = 0;
  TimHandle.Init.CounterMode   = TIM_COUNTERMODE_UP;
  

if(HAL_TIM_PWM_Init(&TimHandle) != HAL_OK)
 	printf("PWM Initialization Error\n");

if(HAL_TIM_PWM_ConfigChannel(&TimHandle, &TimConfig, TIM_CHANNEL_1) != HAL_OK) 
  	printf("PWM Channel 1 Configuration Error \n");

HAL_NVIC_EnableIRQ(TIM15_IRQn); 			
   	printf(" GPIOs and ADC for DC Motor are Initilized \n ");
}


ADD_CMD("init",CmdMTRInit,"		Initialize GPIOs for Motor,ADC,TIM15");








void CmdMotorInput(int mode)
{
  uint32_t PWM_Val;
  char *commandBuffer;

  if(mode != CMD_INTERACTIVE) return;

  if(fetch_string_arg(&commandBuffer))
  {
	printf("Command not recognised\n");
	return;
  }
	
    
     if(strcmp(commandBuffer,"start") == 0)
  {
     if(fetch_uint32_arg(&PWM_Val)) 
	PWM_Val = DUTY_CYCLE_DEFAULT;

     else if(PWM_Val < SPEED_MIN || PWM_Val > SPEED_MAX) 
	PWM_Val = DUTY_CYCLE_DEFAULT;

     speedDC = PWM_Val;

MotorStart();
  }
  else if(strcmp(commandBuffer,"stop") == 0)
  {
 	MotorStop();
  }

  else if(strcmp(commandBuffer,"dump") == 0)
  {
	TimerPWMPrint();
  }

  else printf("Command Not Recognized \n");
}
ADD_CMD("DCM", CmdMotorInput,"<PWM Value>\t DC Motor Commands")

// Reading of tachometers values and printing ADC Errors

void ReadADC(void)
{
  int rc;
  if(HAL_ADC_ConfigChannel(&hadc, &ADCchannel) != HAL_OK) 
	printf("ADC channel configuration failed \n");

	rc = HAL_ADC_Start(&hadc);
  if(rc != HAL_OK) 
	printf("ADC start failed HALstatus: %d\n",rc);

  if(HAL_ADC_PollForConversion(&hadc, 1) != HAL_OK) 
	printf("ADC conversion failed\n");

  ADCData = HAL_ADC_GetValue(&hadc);
}

void MotorStart(void)
{
  if(HAL_TIM_PWM_Start_IT(&TimHandle, TIM_CHANNEL_1) != HAL_OK) 
	printf("PWM Channel 1 Generation Error\n");

  ReadADC();
  TimerPWMPrint();
}


void MotorStop(void)
{
  if(HAL_TIM_PWM_Stop_IT(&TimHandle, TIM_CHANNEL_1) != HAL_OK) 
	printf("PWM Channel 1 Stop Error\n");

  /* Stop ADC conversion, disable ADC peripheral*/
  
  if(HAL_ADC_Stop(&hadc) != HAL_OK) 
	printf("ADC stop failed \n");
  GPIOE->BSRRH = (GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_14 | GPIO_PIN_15);
}

void MotorSpeedControl(void)
{
   static int MtrControl = DCM_START;
   int16_t difference;
   int16_t timCCR;

   if(speedDC > 0)
   {
	timCCR = TIM15->CCR1;			// Capture/Compare register for PWM width 
	if(CurrentSpeedValue <= 0) 
	   MtrControl = DCM_START;		
	switch(MtrControl)
	{
   	   case DCM_START:
	   	if(CurrentSpeedValue < speedDC) 
	   	{
	  	   timCCR += 5;			// pulse step = 5
	      	   if(timCCR > SPEED_MAX)
	      	   {
		 	timCCR = SPEED_MAX;
		 	MtrControl = DCM_ACTIVE;
	      	   }
  	   	}
	   	else 
		    MtrControl = DCM_ACTIVE;
	     	break;
	   case DCM_ACTIVE:
	   	difference = (speedDC - CurrentSpeedValue)/3;	// formula to measure and maintain PWM width
	   	timCCR += difference;
	   	if(timCCR < 1) 
 		   timCCR = 1;
	   	else if(timCCR > SPEED_MAX) 
		   timCCR = SPEED_MAX;
	     	break;
	   default:
	     	break;
	}
	TIM15->CCR1 = timCCR;				// set timCCR value into CCR1 to produce PWM 
    }	
}

// the main code to monitor the DC motor
void Motor_Handler(void)
{
   static uint32_t timeCounterOld = 0;
   CurrentSpeedValue = (ADCData >> 4) + ((ADCData >> 3) % 2);		
	
	if(TIM15->CCR1 >= SPEED_MAX) 
	   BSP_LED_On(LED3); 
	else 
	   BSP_LED_Off(LED3);

	MotorSpeedControl();	
	ReadADC();

	timeCounterOld++;
	if (timeCounterOld > 1000)
	{
		printf("\rTach: 0x%02X\n\tPulse: 0x%02X \n",(unsigned)CurrentSpeedValue,(unsigned)(TIM15->CCR1));
		GPIOE->ODR ^= GPIO_PIN_11; 		// toggle LED to check working of Interrupt
		timeCounterOld = 0;
	}
}


void TimerPWMPrint(void)
{
  printf("ADC buffer:\t %04X \n Pulse:\t %04X \n",(unsigned)ADCData, (unsigned)CurrentSpeedValue); 

void TIM15_IRQHandler(void)
{
   if((TIM15->SR & TIM_FLAG_UPDATE) != 0) 	
 	Motor_Handler();
   HAL_TIM_IRQHandler(&TimHandle);		// producing Interrupt for the Timer
}
}



		

