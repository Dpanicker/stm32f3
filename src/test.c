#include "hd44780.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>

#include "stm32f30x_conf.h"
#include "common.h"

// initialisation of the LCD
void LCD_Init() {

        /* enable clock for PORTC */
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

        /* set GPIOC pins to outputs */
        GPIOC->MODER |= 0b01 << (2 * D4);
        GPIOC->MODER |= 0b01 << (2 * D5);
        GPIOC->MODER |= 0b01 << (2 * D6);
        GPIOC->MODER |= 0b01 << (2 * D7);
        GPIOC->MODER |= 0b01 << (2 * RS);
        GPIOC->MODER |= 0b01 << (2 * EN);

        Delay(30);      /* 300ms */

        HIGH(EN);
        LCD_SetData(3);
        LOW(EN);
        Delay(1);

        HIGH(EN);
        LCD_SetData(3);
        LOW(EN);
        Delay(1);

        HIGH(EN);
        LCD_SetData(3);
        LOW(EN);
        Delay(1);

        HIGH(EN);
        LCD_SetData(2); /* 4 bit mode */
        LOW(EN);
        Delay(1);

        /* init */
        LCD_Command(0x28);      /* 4 bit mode, 2 lines, 5X7 glyph */
        LCD_Command(0x0C);      /* display with no cursor, no blink */
        LCD_Command(0x06);      /* automatic increment, no display shift */
        LCD_Command(0x80);      /* go to address 0 in DDRAM */
        LCD_Command(CLEAR_DISPLAY);
        LCD_Command(GO_HOME);

}


/*
 * send 8bit to LCD
 * @param int type,     0 - LCD command
 *                  1 - LCD data
 */
void LCD_SendByte( uint8_t byte, int RS_mode ) {

        /* set RS */
        if( RS_mode == 1 )      HIGH( RS );
        else LOW( RS );

        /* first the high nibble */
        HIGH( EN );
        LCD_SetData( ( byte >> 4 ) & 0x0F );
        LOW( EN );
        //Delay(1);

        /* then send the low nibble */
        HIGH( EN );
        LCD_SetData( byte & 0x0F);
        LOW( EN );
        //Delay(1);


        if( RS_mode == 1 )
                Delay( 1 );
        else
                Delay( 1 );
}


/*
 * set 4 bit data to lcd port
 */
void LCD_SetData( uint8_t data) {
        if(data & (1 << 0))
                HIGH(D4);
        else
                LOW(D4);

        if(data & (1 << 1))
                HIGH(D5);
        else
                LOW(D5);

        if(data & (1 << 2))
                HIGH(D6);
        else
                LOW(D6);

        if(data & (1 << 3))
                HIGH(D7);
        else
                LOW(D7);
}


/*
 * Lines configuration:
 * lines addresses begin at 0x80
 * 0x00  ..  0x13
 * 0x40  ..  0x53
 * 0x14  ..  0x27
 * 0x54  ..  0x67
 *
 */
void LCD_GotoXY( int x, int y ) {
        /* calculate x position */
        x |= 0b10000000;

        /* add y position */
        if(y == 1)
                x |= 0b01000000;
        else if(y == 2)
                x |= 0b00010100;
        else if(y == 3)
                x |= 0b01010100;

        LCD_Command( x );
}

/*
 * write a string on the display
 */
void LCD_Print( char *text ) {
        int i = 0;
        int len = strlen( (const char *)text );

        for( i = 0; i < len; i++ )
                LCD_Data(text[i]);
}


/*
 * printf function for the LCD screen
 */
void LCD_printf( const char* format, ... ) {
        va_list args;
        char string[50];

        va_start(args, format);
        vsprintf(string, format, args);
        va_end(args);
        LCD_Print(string);
}
Hide details
Change log
r61 by andrei.istodorescu on Nov 28, 2012   Diff
added sources for hd44780 and usart
Go to: 	
Older revisions
All revisions of this file
File info
Size: 2586 bytes, 162 lines
View raw file
