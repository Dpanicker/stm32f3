#include <stdio.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h"
void stepper(uint32_t steps, uint32_t dirx, uint32_t delay);
int delayVal;
GPIO_InitTypeDef GPIO_InitStruct;
uint32_t count=0;
void CmdStepMotor(int mode)
{
GPIO_InitTypeDef GPIO_InitStruct;
if(mode != CMD_INTERACTIVE) return;
__GPIOC_CLK_ENABLE();
/* Configure these ADC pins in analog mode using HAL_GPIO_Init() */
GPIO_InitStruct.Pin = (GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9);
GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
GPIO_InitStruct.Alternate = 0;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}
ADD_CMD("write",CmdStepMotor, " write GPIO");
void Delaye(uint32_t delayVal)
{
TIM1->CNT = 0; /* Reset counter */
while(TIM1->CNT < delayVal)
{
asm volatile ("nop\n");
}
}
void write_gpio(uint32_t val)
{
switch(val){
case 0:
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, 0x01);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, 0x01);
break;
case 1:
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, 0x01);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0x01);
break;
case 2:
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0x01);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 0x01);
break;
case 3:
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, 0x00);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 0x01);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, 0x01);
break;
default:
break;
}
}
void stepper(uint32_t steps, uint32_t dirx, uint32_t delay)
{
while (steps)
{
write_gpio((count %4));
Delaye(delay);
if(dirx>0) //direction is clockwise
{
count++;
}
else
{
count--; //directionis anticlockwise
}
steps--;
}
}
void StepMotor(int mode)
{
if(mode != CMD_INTERACTIVE) return;
uint32_t steps,delay;
uint32_t dirx;
fetch_uint32_arg(&steps);
fetch_uint32_arg(&dirx);
fetch_uint32_arg(&delay);
stepper(steps,dirx, delay);
}
ADD_CMD("steps",StepMotor," <steps> <dirx> <delay>")
