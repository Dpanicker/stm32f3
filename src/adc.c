#include <stdio.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h"
#include "stm32f3xx_hal_adc_ex.h"
#include "stm32f3xx_hal_adc.h"
HAL_StatusTypeDef rc ;
//uint32_t val;
ADC_HandleTypeDef hadc ;
//ADC_CommonInitTypeDef hadc;
GPIO_InitTypeDef GPIO_InitStruct;
void CmdAdc(int mode)
{
if (mode!=CMD_INTERACTIVE)return;
/* Enable the ADC interface clock using __ADC_CLK_ENABLE() */
__ADC1_CLK_ENABLE();
__HAL_RCC_ADC12_CONFIG(RCC_ADC12PLLCLK_DIV1);
/* ADC pins configuration
Enable the clock for the ADC GPIOs */
__GPIOA_CLK_ENABLE();
__GPIOF_CLK_ENABLE();
/* Configure these ADC pins in analog mode using HAL_GPIO_Init() */
GPIO_InitStruct.Pin = (GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
GPIO_InitStruct.Alternate = 0;
HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
GPIO_InitStruct.Pin = (GPIO_PIN_4);
GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
GPIO_InitStruct.Alternate = 0;
HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);
/* Initialize ADC */
hadc.Instance = ADC1;
hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
hadc.Init.Resolution = ADC_RESOLUTION12b;
hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
hadc.Init.EOCSelection = EOC_SINGLE_CONV;
hadc.Init.LowPowerAutoWait = DISABLE;
hadc.Init.ContinuousConvMode = DISABLE;
hadc.Init.NbrOfConversion = 1;
hadc.Init.DiscontinuousConvMode = DISABLE;
hadc.Init.NbrOfDiscConversion = 0;
hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
hadc.Init.ExternalTrigConvEdge = 0;
hadc.Init.DMAContinuousRequests = DISABLE;
hadc.Init.Overrun = OVR_DATA_OVERWRITTEN;
rc = HAL_ADC_Init(&hadc);
if(rc != HAL_OK) {
printf("ADC1 initialization failed with rc=%u\n",rc);
}
}
ADD_CMD("adcinit",CmdAdc," ADC Initiation ")
void CmdAdcRead(int mode){
//ADC reading:
uint32_t val=0;
HAL_StatusTypeDef rc;
ADC_ChannelConfTypeDef config;
int channel=2;
//int val1;
/* Configure the selected channel */
config.Channel = channel;
config.Rank = 1; /* Rank needs to be 1, otherwise no conversion is done */
config.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
config.SingleDiff = ADC_SINGLE_ENDED;
config.OffsetNumber = ADC_OFFSET_NONE;
config.Offset = 0;
rc = HAL_ADC_ConfigChannel(&hadc,&config);
if(rc != HAL_OK) {
printf("ADC channel configure failed with rc=%u\n",(unsigned)rc);
return ;
}
/* Start the ADC peripheral */
rc = HAL_ADC_Start(&hadc);
if(rc != HAL_OK) {
printf("ADC start failed with rc=%u\n",(unsigned)rc);
return ;
}
/* Wait for end of conversion */
rc = HAL_ADC_PollForConversion(&hadc, 100);
if(rc != HAL_OK) {
printf("ADC poll for conversion failed with rc=%u\n",(unsigned)rc);
return ;
}
/* Read the ADC converted values */
val = HAL_ADC_GetValue(&hadc);
printf("value of the read adc,%u",(unsigned)val);
//val1=val1+1;
/* Stop the ADC peripheral */
rc = HAL_ADC_Stop(&hadc);
if(rc != HAL_OK) {
printf("ADC stop failed with rc=%u\n",(unsigned)rc);
return ;
}
}
ADD_CMD("adcread",CmdAdcRead," ADC read ")
